# Notion-Actions

This service does introduce two endpoints to interact with notion.

## Available endpoints

Endpoint to reschedule overdue tasks in notion.
```
GET http://localhost:8090/Reschedule/Tasks
```

Endpoint to replace each ` -` with a new line in notion text properties.
```
GET http://localhost:8090/Replace/DailyPropertyNewLines
```

## Configuration

Provide a `.env` file with the following configs:

```
NotionOptions__AuthToken=
NotionOptions__TaskDatabaseId=
NotionOptions__DailyDatabaseId=
```
