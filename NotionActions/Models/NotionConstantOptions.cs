namespace NotionActions.Models;

/// <summary>
/// Options for notion property identifications.
/// </summary>
public class NotionConstantOptions
{
    /// <summary>
    /// Name of the date property that represents the do date of a task.
    /// This field will be changed to the current date, if the date is overdue.
    /// </summary>
    public string DoDatePropertyName { get; set; } = null!;

    /// <summary>
    /// Id of the date property that represents the do date of a task.
    /// This field will be changed to the current date, if the date is overdue.
    /// </summary>
    public string DoDatePropertyId { get; set; } = null!;

    /// <summary>
    /// Name of the status property of the tasks. Only tasks not already done
    /// will be changed.
    /// </summary>
    public string StatusPropertyName { get; set; } = null!;

    /// <summary>
    /// The status that represents, that the task is already done.
    /// </summary>
    public string StatusFinishedName { get; set; } = null!;
    
    /// <summary>
    /// Name of the date property in the daily database
    /// </summary>
    public string DatePropertyName { get; set; } = null!;

    /// <summary>
    /// Placeholder which will be replaced by a line break.
    /// </summary>
    public string LineBreakPlaceholder { get; set; } = null!;
}