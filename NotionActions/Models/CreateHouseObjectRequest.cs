namespace NotionActions.Models;

/// <summary>
/// Request object to create a house object.
/// </summary>
/// <param name="ObjectName">The name of the house object.</param>
public record class CreateHouseObjectRequest(string ObjectName);
