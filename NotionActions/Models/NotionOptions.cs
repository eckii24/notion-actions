namespace NotionActions.Models;

/// <summary>
/// Notion options about the secrets and database ids.
/// This settings will be different for each user!
/// </summary>
public class NotionOptions
{
    /// <summary>
    /// The auth token to access the notion API.
    /// </summary>
    public string AuthToken { get; set; } = null!;

    /// <summary>
    /// The id of the tasks database.
    /// </summary>
    public string? TaskDatabaseId { get; set; }

    /// <summary>
    /// The id of the daily database.
    /// </summary>
    public string? DailyDatabaseId { get; set; }

    /// <summary>
    /// The id of the object database.
    /// </summary>
    public string? ObjectDatabaseId { get; set; }
    
    /// <summary>
    /// The id of the object question database.
    /// </summary>
    public string? ObjectQuestionDatabaseId { get; set; }

    /// <summary>
    /// The id of the obejct question map database.
    /// </summary>
    public string? ObjectQuestionMapDatabaseId { get; set; }
}