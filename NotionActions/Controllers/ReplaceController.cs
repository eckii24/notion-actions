using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NotionActions.Models;
using NotionActions.Services;

namespace NotionActions.Controller;

/// <summary>
/// Controller to replace things in notions
/// </summary>
[ApiController]
[Route("[controller]")]
public class ReplaceController : ControllerBase
{
    private readonly IReplaceService _replaceService;

    public ReplaceController(IReplaceService replaceService)
    {
        _replaceService = replaceService;
    }

    /// <summary>
    /// Daily Property New Lines
    /// </summary>
    /// <remarks>
    /// Loads the dailies from the last three days and checks,
    /// if any rich text field has a " -" in it. 
    /// This will be replaced by a line break;
    /// </remarks>
    [HttpGet("[action]")]
    public async Task<ActionResult> DailyPropertyNewLines()
    {
        var replacementCount = await _replaceService.ReplaceDailyPropertyNewLines();

        return Ok(new { message = $"Replaced '{replacementCount}' page properties." });
    }
}