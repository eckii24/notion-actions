using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NotionActions.Models;
using NotionActions.Services;

namespace NotionActions.Controller;

/// <summary>
/// Controller to reschedule things in notions
/// </summary>
[ApiController]
[Route("[controller]")]
public class RescheduleController : ControllerBase
{
    private readonly IRescheduleService _rescheduleService;

    public RescheduleController(IRescheduleService rescheduleService)
    {
        _rescheduleService = rescheduleService;
    }

    /// <summary>
    /// Reschedule tasks
    /// </summary>
    /// <remarks>
    /// Gets all tasks from the configured Tasks database that are overdue
    /// and changes the date to the current date
    /// </remarks>
    [HttpGet("[action]")]
    public async Task<ActionResult> Tasks()
    {
        var rescheduledTaskCount = await _rescheduleService.RescheduleOverdueTasks();

        return Ok(new { message = $"Rescheduled '{rescheduledTaskCount}' overdue tasks in Notion." });
    }
}