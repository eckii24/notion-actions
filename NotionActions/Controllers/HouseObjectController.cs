using Microsoft.AspNetCore.Mvc;
using NotionActions.Models;
using NotionActions.Services;

namespace NotionActions.Controllers;

/// <summary>
/// Action to interact with house objects and object questions.
/// </summary>
[ApiController]
[Route("[controller]")]
public class HouseObjectController(IHouseObjectService houseObjectService) : ControllerBase
{
    /// <summary>
    /// Create Object by Name
    /// </summary>
    /// <remarks>
    /// Create a new object and adds all available questions to the mapping table.
    /// If an object with the same name already exists, it will only add missing questions to the mapping.
    /// </remarks>
    /// <param name="request">The request object with the object name.</param>
    /// <returns>OK, to indicate success.</returns>
    [HttpPost]
    public async Task<ActionResult> CreateObjectByName([FromBody] CreateHouseObjectRequest request)
    {
        await houseObjectService.CreateObjectByName(request.ObjectName);

        return Ok(new {message = "Object successfully created."});
    }
}