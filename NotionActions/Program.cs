using NotionActions.Models;
using NotionActions.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services
  .AddOptions<NotionOptions>()
  .Bind(builder.Configuration.GetSection(nameof(NotionOptions)))
  .ValidateDataAnnotations();
builder.Services
  .AddOptions<NotionConstantOptions>()
  .Bind(builder.Configuration.GetSection(nameof(NotionConstantOptions)))
  .ValidateDataAnnotations();

var notionOptions = builder.Configuration.GetSection(nameof(NotionOptions)).Get<NotionOptions>();
ArgumentNullException.ThrowIfNull(notionOptions);

builder.Services.AddNotionClient(options =>
{
  options.AuthToken = notionOptions.AuthToken;
});

builder.Services.AddTransient<IRescheduleService, RescheduleService>();
builder.Services.AddTransient<IReplaceService, ReplaceService>();
builder.Services.AddTransient<IHouseObjectService, HouseObjectService>();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.MapControllers();

app.Run();
