using System;

namespace NotionActions.Services;

/// <summary>
/// Service to handle the creation of objects and object question maps.
/// Those databases are created to handle house visits in a structured way.
/// </summary>
public interface IHouseObjectService
{
    /// <summary>
    /// Create a new object by name.
    /// </summary>
    /// <param name="name">The name of the object.</param>
    /// <returns>The ID of the generated object.</returns>
    public Task CreateObjectByName(string name);
}
