namespace NotionActions.Services;

/// <summary
/// Service to interact with the notion API to reschedule things in notion.
/// </summary>
public interface IRescheduleService {
    /// <summary>
    /// Reschedule overdue tasks from the tasks database in notion.
    /// </summary>
    public Task<int> RescheduleOverdueTasks();
}