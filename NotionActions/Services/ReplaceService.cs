using Microsoft.Extensions.Options;
using Notion.Client;
using NotionActions.Models;

namespace NotionActions.Services;

public class ReplaceService : IReplaceService
{
    private readonly NotionOptions _notionOptions;
    private readonly NotionConstantOptions _notionConstantOptions;
    private readonly INotionClient _notionClient;

    public ReplaceService(
        IOptions<NotionOptions> notionOptions,
        IOptions<NotionConstantOptions> notionConstantOptions, INotionClient notionClient)
    {
        _notionOptions = notionOptions.Value;
        _notionConstantOptions = notionConstantOptions.Value;
        _notionClient = notionClient;
    }

    public async Task<int> ReplaceDailyPropertyNewLines()
    {
        var relevantPages = await GetPagesOfRelevantDays();

        var pageObjectsToUpdate = relevantPages
            .Select(ResolveUpdatedProperties)
            .Where(page => page.PageProperties.Any());

        await UpdatePageObjects(pageObjectsToUpdate);

        return pageObjectsToUpdate
            .Select(page => page.PageProperties)
            .Count();
    }

    private async Task<IEnumerable<Page>> GetPagesOfRelevantDays()
    {
        var endDate = DateTime.Today;
        var startDate = endDate - TimeSpan.FromDays(2);

        var endDateFilter = new DateFilter(
            _notionConstantOptions.DatePropertyName,
            onOrBefore: endDate);
        var startDateFilter = new DateFilter(
            _notionConstantOptions.DatePropertyName,
            onOrAfter: startDate);
        var dateFilter = new CompoundFilter { And = new List<Filter> { endDateFilter, startDateFilter } };
        var databaseQuery = new DatabasesQueryParameters { Filter = dateFilter };

        var relevantDays = await _notionClient.Databases.QueryAsync(
            _notionOptions.DailyDatabaseId,
            databaseQuery);

        return relevantDays.Results;
    }

    private (string PageId, IDictionary<string, PropertyValue> PageProperties) ResolveUpdatedProperties(Page page)
    {
        // Only RichText properties are relevant
        var relevantProperties = page.Properties
            .Where(prop => prop.Value is RichTextPropertyValue)
            .Select(prop => new KeyValuePair<string, RichTextPropertyValue>(prop.Key, (RichTextPropertyValue)prop.Value));

        // Only RichTexts with the string " - " are relevant
        // These are the texts without the correct linebreak
        var propertiesToUpdate = relevantProperties
            .Where(prop => prop.Value.RichText.Any(text => text.PlainText.Contains(" - ")));

        // Add the missing line breaks by replace the string " - "
        var updatedProperties = propertiesToUpdate
            .Select(prop =>
            {
                prop.Value.RichText = prop.Value.RichText
                    .Select(text =>
                    {
                        var richText = (RichTextText)text;
                        richText.Text.Content = richText.Text.Content.Replace(
                            _notionConstantOptions.LineBreakPlaceholder, 
                            "\n- ");

                        return (RichTextBase)richText;
                    })
                    .ToList();

                return prop;
            });

        var updatedPropertiesAsDict = updatedProperties
            .ToDictionary(prop => prop.Key, prop => (PropertyValue)prop.Value);

        return (page.Id, updatedPropertiesAsDict);
    }

    private async Task UpdatePageObjects(IEnumerable<(string PageId, IDictionary<string, PropertyValue> PageProperties)> pageObjectsToUpdate)
    {
        foreach (var (PageId, PageProperties) in pageObjectsToUpdate)
        {
            await _notionClient.Pages.UpdatePropertiesAsync(PageId, PageProperties);
        }
    }

}
