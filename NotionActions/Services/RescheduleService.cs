using Microsoft.Extensions.Options;
using Notion.Client;
using NotionActions.Models;

namespace NotionActions.Services;

/// <inheritdoc />
public class RescheduleService : IRescheduleService
{
    private readonly NotionOptions _notionOptions;
    private readonly NotionConstantOptions _notionConstantOptions;
    private readonly INotionClient _notionClient;

    public RescheduleService(
        IOptions<NotionOptions> notionOptions,
        IOptions<NotionConstantOptions> notionConstantOptions,
        INotionClient notionClient)
    {
        _notionOptions = notionOptions.Value;
        _notionConstantOptions = notionConstantOptions.Value;
        _notionClient = notionClient;
    }

    /// <inheritdoc />
    public async Task<int> RescheduleOverdueTasks()
    {
        var overduePageIds = await GetPageIdsOfOverdueTasks();

        await UpdateOverduePages(overduePageIds);

        return overduePageIds.Count();
    }

    private async Task<IEnumerable<string>> GetPageIdsOfOverdueTasks()
    {
        var dateFilter = new DateFilter(
            _notionConstantOptions.DoDatePropertyName,
            before: DateTime.Today);
        var statusFilter = new StatusFilter(
            _notionConstantOptions.StatusPropertyName,
            doesNotEqual: _notionConstantOptions.StatusFinishedName);

        var andFilter = new CompoundFilter { And = new List<Filter> { dateFilter, statusFilter } };

        var databaseQuery = new DatabasesQueryParameters { Filter = andFilter };

        var overdueTasks = await _notionClient.Databases.QueryAsync(
            _notionOptions.TaskDatabaseId,
            databaseQuery);

        return overdueTasks.Results.Select(page => page.Id);
    }

    private async Task UpdateOverduePages(IEnumerable<string> overduePageIds)
    {
        foreach (var pageId in overduePageIds)
        {
            var dateValue = new DatePropertyValue
            {
                Id = _notionConstantOptions.DoDatePropertyId,
                Date = new Date() { Start = DateTime.Now.Date }
            };
            var updates = new Dictionary<string, PropertyValue> {
                { _notionConstantOptions.DoDatePropertyName ?? string.Empty, dateValue } };

            await _notionClient.Pages.UpdatePropertiesAsync(pageId, updates);
        }
    }
}