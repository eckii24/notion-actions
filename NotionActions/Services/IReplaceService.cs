namespace NotionActions.Services;

/// <summary>
/// Service to replace properties in notion databases
/// </summary>
public interface IReplaceService
{
    /// <summary>
    /// Replace any " -" with a line break in rich text fields,
    /// for the last three days
    /// </summary>
    public Task<int> ReplaceDailyPropertyNewLines();
}