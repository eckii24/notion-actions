using System;
using Microsoft.Extensions.Options;
using Notion.Client;
using NotionActions.Models;

namespace NotionActions.Services;

/// <inheritdoc />
public class HouseObjectService : IHouseObjectService
{
    private readonly NotionOptions _notionOptions;
    private readonly INotionClient _notionClient;

    public HouseObjectService(
        IOptions<NotionOptions> notionOptions,
        INotionClient notionClient)
    {
        _notionOptions = notionOptions.Value;
        _notionClient = notionClient;
    }

    /// <inheritdoc />
    public async Task CreateObjectByName(string name)
    {
        // TODOs:
        // - Komplette Abstraktion, da DB-Ids und Spalten-Namen als Parameter übergeben werden
        ArgumentNullException.ThrowIfNullOrWhiteSpace(_notionOptions.ObjectQuestionDatabaseId);

        var objectPage = await GetOrCreateObject(name);

        var questionMapsPageIds = await GetAlreadyMappedQuestionIds(objectPage.Id);
        var questions = await GetAllItems(_notionOptions.ObjectQuestionDatabaseId);
        var newQuestion = questions.Where(q => !questionMapsPageIds.Contains(q.Id));

        await Task.WhenAll(newQuestion.Select(q => CreateObjectQuestionMap(objectPage, q)));
    }

    private async Task<Page> GetOrCreateObject(string name)
    {
        ArgumentNullException.ThrowIfNullOrWhiteSpace(_notionOptions.ObjectDatabaseId);

        var pages = await GetAllItems(
            _notionOptions.ObjectDatabaseId,
            new DatabasesQueryParameters { Filter = new TitleFilter("Name", name) });

        if (pages.Count == 1)
        {
            return pages[0];
        }

        if (pages.Count > 1)
        {
            throw new InvalidDataException("Name is not unique and already exists multiple times...");
        }

        var createParams = PagesCreateParametersBuilder
            .Create(new DatabaseParentInput { DatabaseId = _notionOptions.ObjectDatabaseId })
            .AddProperty(
                "Name",
                new TitlePropertyValue { Title = [new RichTextText { Text = new Text { Content = name } }] })
            .Build();

        return await _notionClient.Pages.CreateAsync(createParams);
    }

    private async Task<List<string>> GetAlreadyMappedQuestionIds(string objectPageId)
    {
        ArgumentNullException.ThrowIfNullOrWhiteSpace(_notionOptions.ObjectQuestionMapDatabaseId);

        var maps = await GetAllItems(
            _notionOptions.ObjectQuestionMapDatabaseId,
            new DatabasesQueryParameters { Filter = new RelationFilter("Objekte", objectPageId) });

        return maps
            .Select(map => map.Properties.Single(prop => prop.Key == "Objekt-Fragen").Value)
            .OfType<RelationPropertyValue>()
            .SelectMany(rel => rel.Relation)
            .Select(rel => rel.Id)
            .ToList();
    }

    private async Task CreateObjectQuestionMap(Page objectPage, Page question)
    {
        var titleProperty = (TitlePropertyValue)question.Properties.Single(prop => prop.Value is TitlePropertyValue).Value;
        var questionTitle = string.Join(
            string.Empty,
            titleProperty.Title.Select(richTextbase => richTextbase.PlainText));

        var createMapParams = PagesCreateParametersBuilder
            .Create(new DatabaseParentInput { DatabaseId = _notionOptions.ObjectQuestionMapDatabaseId })
            .AddProperty(
                "Name",
                new TitlePropertyValue { Title = [new RichTextText { Text = new Text { Content = questionTitle } }] })
            .AddProperty(
                "Objekte",
                new RelationPropertyValue { Relation = [new ObjectId() { Id = objectPage.Id }] })
            .AddProperty(
                "Objekt-Fragen",
                new RelationPropertyValue { Relation = [new ObjectId() { Id = question.Id }] })
            .Build();

        await _notionClient.Pages.CreateAsync(createMapParams);
    }

    private async Task<List<Page>> GetAllItems(
        string databaseId,
        DatabasesQueryParameters? parameters = null,
        List<Page>? results = null)
    {
        results ??= new List<Page>();
        parameters ??= new DatabasesQueryParameters();

        var result = await _notionClient.Databases.QueryAsync(databaseId, parameters);

        results.AddRange(result.Results);

        if (result.HasMore)
        {
            parameters.StartCursor = result.NextCursor;

            return await GetAllItems(databaseId, parameters, results);
        }

        return results;
    }
}
